/**
 * VERSION 1.1
 * DATE : 05/02/2021
 * Last Update : 29/04/2021
 * @Author : Etienne
 */

var express = require("express");
var router = express.Router();
var pgp = require("pg-promise")();
var db = pgp(process.env.DATABASE);
console.log("Connexion réussie à la base de données");
const request = require("request");

// Difference entre les fuseaux horaires
var diff = 0;

// Objet Spotify Pochettes
var old;
var current;
var currenturl;
var next;
var nexturl;
var resultat;
var firstTime = true;
var headers;

var nbAuditeurs;

const options = {
  year: "numeric",
  month: "2-digit",
  day: "2-digit",
  hour: "2-digit",
  minute: "2-digit",
  second: "2-digit",
};

/**
 * Permet de Clean la requete Spotify
 * @param string
 * @returns {string}
 */
function cleanQuery(string) {
  console.log("String before cleaning : " + string);
  let stringquery = string;
  stringquery = stringquery.toLowerCase();
  stringquery = stringquery.split(";", 1).join();
  stringquery = stringquery.split("&#039;").join("");
  stringquery = stringquery.split("&amp;").join("");
  stringquery = stringquery.split("&quot;").join('"');
  stringquery = stringquery.split("/").join(" ");
  stringquery = stringquery.split("amp").join("");
  stringquery = stringquery.split(")").join("");
  stringquery = stringquery.split("(").join("");
  stringquery = stringquery.split("é").join("e");
  stringquery = stringquery.split("ó").join("o");
  stringquery = stringquery.split("℃").join("");
  stringquery = stringquery.split("ù").join("u");
  stringquery = stringquery.split("û").join("u");
  stringquery = stringquery.split("&").join("");
  stringquery = stringquery.split("á").join("a");
  stringquery = stringquery.split("ý").join("y");
  stringquery = stringquery.split("í").join("i");
  stringquery = stringquery.split("ř").join("r");
  stringquery = stringquery.split("ð").join("d");
  stringquery = stringquery.split("ö").join("o");
  stringquery = stringquery.split("ø").join("o");
  stringquery = stringquery.split("ę").join("e");
  stringquery = stringquery.split("ł").join("l");
  stringquery = stringquery.split("ś").join("s");
  stringquery = stringquery.split("ñ").join("n");
  stringquery = stringquery.split("ê").join("e");
  stringquery = stringquery.split("ï").join("i");
  stringquery = stringquery.split("à").join("a");
  stringquery = stringquery.split("è").join("e");
  stringquery = stringquery.split("ü").join("u");
  stringquery = stringquery.split("ê").join("e");
  stringquery = stringquery.split("ç").join("c");
  stringquery = stringquery.split("ô").join("o");
  stringquery = stringquery.split("ć").join("c");
  stringquery = stringquery.replace(/\W\s/g, " ");
  stringquery = stringquery.split("%21").join("");
  stringquery = stringquery.split("æ").join("");
  stringquery = stringquery.split("ú").join("u");
  stringquery = stringquery.split("®").join("");
  stringquery = stringquery.split("ō").join("o");
  stringquery = stringquery.split("ā").join("a");
  stringquery = stringquery.split("0").join("");
  stringquery = stringquery.split("1").join("");
  stringquery = stringquery.split("2").join("");
  stringquery = stringquery.split("3").join("");
  stringquery = stringquery.split("4").join("");
  stringquery = stringquery.split("5").join("");
  stringquery = stringquery.split("6").join("");
  stringquery = stringquery.split("7").join("");
  stringquery = stringquery.split("8").join("");
  stringquery = stringquery.split("9").join("");
  console.log("String after cleaning : " + stringquery);
  return stringquery;
}

function ArtistCleaning(string) {
  let stringquery = string;
  stringquery = stringquery.split("/", 1).join();
  stringquery = stringquery.split(";", 1).join();
  return stringquery;
}

require("dotenv").config();

function recupSpotify() {
  nbAuditeurs = 0;
  request(process.env.APICHAOS, "", function (error, response, body) {
    if (!error && response.statusCode === 200) {
      setTimeout(() => {
        console.log("Nombre d'auditeurs : " + nbAuditeurs);
      }, 10000);
      try {
        resultat = JSON.parse(body);
      } catch {
        console.log("Error Json Parse Body Chaos Radio");
      }
      const oldsongartist = resultat.previous.metadata.artist_name;
      const oldsongalbum = resultat.previous.metadata.album_title;
      const currentsongartist = resultat.current.metadata.artist_name;
      const currentsongalbum = resultat.current.metadata.album_title;
      const nextsongartist = resultat.next.metadata.artist_name;
      const nextsongalbum = resultat.next.metadata.album_title;
      const owner = resultat.next.metadata.owner_id; // 2 minutes = id
      const currentsongend = Date.parse(resultat.current.ends);
      if (firstTime) {
        diff = new Date().getTimezoneOffset() * 60 * 1000;
      }
      setInterval(() => {
        diff = new Date().getTimezoneOffset() * 60 * 1000;
      }, 3600000);
      resultat.previous.metadata.artwork_url = null;
      resultat.current.metadata.artwork_url = null;
      resultat.next.metadata.artwork_url = null;
      let finishP;
      if (diff < 0) {
        finishP = currentsongend - diff - new Date();
      } else {
        finishP = currentsongend + diff - new Date();
      }
      setTimeout(() => recupSpotify(), finishP + 2000);
      if (firstTime) {
        firstTime = false;
        // Recuperation album previous

        const clientId = process.env.SPOTIFY_API_ID;
        const clientSecret = process.env.SPOTIFY_CLIENT_SECRET;
        const encodedData = Buffer.from(clientId + ":" + clientSecret).toString(
          "base64"
        );
        headers = {
          Authorization: "Basic " + encodedData,
        };
        request(
          process.env.SPOTIFY_TOKEN,
          {
            headers: headers,
            method: "POST",
            form: { grant_type: "client_credentials" },
          },
          function (error, response, body) {
            const data = JSON.parse(body);
            const key = data.access_token;
            const type = data.token_type;
            if (oldsongalbum && oldsongartist) {
              const album = cleanQuery(oldsongalbum);
              const artist = cleanQuery(oldsongartist);
              const query = "album:" + album + " artist:" + artist;
              const q = "q=" + query + "&type=album&limit=1";
              const header = {
                Authorization: type + " " + key,
                Accept: "application/json",
              };
              request(
                "https://api.spotify.com/v1/search?" + q,
                {
                  headers: header,
                  method: "GET",
                },
                function (error, response, body) {
                  if (!error && response.statusCode === 200) {
                    old = JSON.parse(body);
                    if (old.albums.items[0]) {
                      resultat.previous.metadata.artwork_url =
                        old.albums.items[0].images[1].url;
                    } else {
                      resultat.previous.metadata.artwork_url = null;
                    }
                  } else {
                    console.log("Request Error : ");
                    console.log(error);
                    old = null;
                    resultat.previous.metadata.artwork_url = null;
                  }
                }
              );
            } else {
              old = null;
            }
          }
        );

        // Recuperation current album
        request(
          process.env.SPOTIFY_TOKEN,
          {
            headers: headers,
            method: "POST",
            form: { grant_type: "client_credentials" },
          },
          function (error, response, body) {
            const data = JSON.parse(body);
            const key = data.access_token;
            const type = data.token_type;

            if (currentsongalbum && currentsongartist) {
              const album = cleanQuery(currentsongalbum);
              const artist = cleanQuery(currentsongartist);
              const query = "album:" + album + " artist:" + artist;
              const q = "q=" + query + "&type=album&limit=1";
              const header = {
                Authorization: type + " " + key,
                Accept: "application/json",
              };
              request(
                "https://api.spotify.com/v1/search?" + q,
                {
                  headers: header,
                  method: "GET",
                },
                function (error, response, body) {
                  if (!error && response.statusCode === 200) {
                    current = JSON.parse(body);
                    if (current.albums.items[0]) {
                      resultat.current.metadata.artwork_url =
                        current.albums.items[0].images[1].url;
                      setTimeout(
                        () =>
                          (currenturl = current.albums.items[0].images[1].url),
                        1000
                      );
                    } else {
                      resultat.current.metadata.artwork_url = null;
                      currenturl = null;
                    }
                  } else {
                    console.log("Erreur : //////////");
                    console.log(error);
                    current = null;
                    resultat.current.metadata.artwork_url = null;
                    currenturl = null;
                  }
                }
              );
            } else {
              current = null;
              currenturl = null;
            }
          }
        );

        // Recuperation next Album
        request(
          process.env.SPOTIFY_TOKEN,
          {
            headers: headers,
            method: "POST",
            form: { grant_type: "client_credentials" },
          },
          function (error, response, body) {
            const data = JSON.parse(body);
            const key = data.access_token;
            const type = data.token_type;

            if (nextsongalbum && nextsongartist) {
              const album = cleanQuery(nextsongalbum);
              const artist = cleanQuery(nextsongartist);
              const query = "album:" + album + " artist:" + artist;
              const q = "q=" + query + "&type=album&limit=1";
              const header = {
                Authorization: type + " " + key,
                Accept: "application/json",
              };
              request(
                "https://api.spotify.com/v1/search?" + q,
                {
                  headers: header,
                  method: "GET",
                },
                function (error, response, body) {
                  if (!error && response.statusCode === 200) {
                    next = JSON.parse(body);
                    if (next.albums.items[0]) {
                      resultat.next.metadata.artwork_url =
                        next.albums.items[0].images[1].url;
                      setTimeout(
                        () => (nexturl = next.albums.items[0].images[1].url),
                        1000
                      );
                    } else {
                      resultat.next.metadata.artwork_url;
                      nexturl = null;
                    }
                  } else {
                    console.log("Erreur : //////////");
                    console.log(error);
                    next = null;
                    resultat.next.metadata.artwork_url = null;
                    nexturl = null;
                  }
                }
              );
            } else {
              next = null;
              nexturl = null;
            }
          }
        );
      } else if (!firstTime) {
        console.log("REQ BEGIN");
        resultat.previous.metadata.artwork_url = currenturl;
        resultat.current.metadata.artwork_url = nexturl;
        if (owner === 10) {
          console.log("2 MIN");
          console.log("REQ END");
          console.log("------------------------------------------------------");
          resultat.next.metadata.artwork_url = "assets/img/2min.jpg";
          resultat.next.metadata.artist_name = "Les 2 Minutes du peuple";
          setTimeout(() => (currenturl = nexturl), 500);
          setTimeout(() => (nexturl = "assets/img/2min.jpg"), 2000);
        } else if (owner === 8) {
          console.log("CAKOS");
          console.log("REQ END");
          console.log("------------------------------------------------------");
          resultat.next.metadata.artwork_url = "assets/img/CAKOS.jpg";
          setTimeout(() => (currenturl = nexturl), 500);
          setTimeout(() => (nexturl = "assets/img/CAKOS.jpg"), 2000);
        } else if (owner === 3) {
          console.log("JINGLE");
          console.log("REQ END");
          console.log("------------------------------------------------------");
          resultat.next.metadata.artwork_url = null;
          setTimeout(() => (currenturl = nexturl), 500);
          setTimeout(() => (nexturl = null), 2000);
        } else {
          resultat.previous.metadata.artwork_url = currenturl;
          resultat.current.metadata.artwork_url = nexturl;
          setTimeout(() => (currenturl = nexturl), 500);
          const clientId = process.env.SPOTIFY_API_ID;
          const clientSecret = process.env.SPOTIFY_CLIENT_SECRET;
          const encodedData = Buffer.from(
            clientId + ":" + clientSecret
          ).toString("base64");
          const headers = {
            Authorization: "Basic " + encodedData,
          };
          request(
            process.env.SPOTIFY_TOKEN,
            {
              headers: headers,
              method: "POST",
              form: { grant_type: "client_credentials" },
            },
            function (error, response, body) {
              const data = JSON.parse(body);
              const key = data.access_token;
              const type = data.token_type;

              if (nextsongartist && nextsongalbum) {
                const album = cleanQuery(nextsongalbum);
                const artist = cleanQuery(nextsongartist);
                const query = "album:" + album + " artist:" + artist;
                console.log(query);
                const q = "q=" + query + "&type=album&limit=1";
                const header = {
                  Authorization: type + " " + key,
                  Accept: "application/json",
                };
                request(
                  "https://api.spotify.com/v1/search?" + q,
                  {
                    headers: header,
                    method: "GET",
                  },
                  function (error, response, body) {
                    if (!error && response.statusCode === 200) {
                      next = JSON.parse(body);
                      console.log("SPOTIFY REQ");
                      if (
                        next.album &&
                        next.albums.items[0] &&
                        next.albums.items[0].images.length >= 1
                      ) {
                        console.log("REQ OK");
                        console.log("REQ END");
                        console.log(
                          "------------------------------------------------------"
                        );
                        resultat.next.metadata.artwork_url =
                          next.albums.items[0].images[1].url;
                        setTimeout(
                          () => (nexturl = next.albums.items[0].images[1].url),
                          2000
                        );
                      } else {
                        const artistsolo = ArtistCleaning(nextsongartist);
                        resultat.next.metadata.artwork_url = null;
                        console.log("REQ WARNING");
                        const query = "artist:" + artistsolo;
                        console.log(query);
                        const q = "q=" + query + "&type=artist&limit=1";
                        const header = {
                          Authorization: type + " " + key,
                          Accept: "application/json",
                        };
                        request(
                          "https://api.spotify.com/v1/search?" + q,
                          {
                            headers: header,
                            method: "GET",
                          },
                          function (error, response, body) {
                            if (!error && response.statusCode === 200) {
                              next = JSON.parse(body);
                              console.log("REQ ARTIST");
                              console.log(next.artists.href);
                              if (
                                next.artists.items[0] &&
                                next.artists.items[0].images.length >= 1
                              ) {
                                console.log("REQ ARTIST OK");
                                console.log("REQ END");
                                console.log(
                                  "------------------------------------------------------"
                                );
                                resultat.next.metadata.artwork_url =
                                  next.artists.items[0].images[1].url;
                                setTimeout(
                                  () =>
                                    (nexturl =
                                      next.artists.items[0].images[1].url),
                                  2000
                                );
                              } else {
                                resultat.next.metadata.artwork_url = null;
                                console.error(
                                  "ERROR ---------------------------"
                                );
                                console.error(
                                  "ARTIST : " + artistsolo + " ALBUM : " + album
                                );
                                console.error(
                                  "ARTIS BEFORE CLEANING : " +
                                    nextsongartist +
                                    " ALBUM BEFORE CLEANING : " +
                                    nextsongalbum
                                );
                                console.error(
                                  "END ERROR ---------------------------------"
                                );
                                console.log("REQ END");
                                console.log(
                                  "------------------------------------------------------"
                                );
                                setTimeout(() => (nexturl = null), 2000);
                              }
                            }
                          }
                        );
                      }
                    } else {
                      console.error(
                        "ERROR : ---------------------------------"
                      );
                      console.error("REQ : " + q);
                      console.error(
                        "END ERROR : ---------------------------------"
                      );
                      console.log("REQ END");
                      console.log(
                        "------------------------------------------------------"
                      );
                      setTimeout(() => {
                        next = null;
                        resultat.next.metadata.artwork_url = null;
                        nexturl = null;
                      }, 2000);
                    }
                  }
                );
              } else {
                next = null;
                nexturl = null;
                setTimeout(() => (currenturl = nexturl), 2000);
                console.log("---------------------------------");
                console.log("NO REQUEST ELEMENTS");
                console.log("REQ END");
                console.log("---------------------------------");
              }
            }
          );
        }
      }
    } else {
      console.log("ERROR API CHAOS : ---------------");
      console.log(error);
      console.log("------------------------------------------------------");
      setTimeout(() => recupSpotify(), 5000);
    }
  });
}

/**
 *  GET method to retrieve all votes, order by dates, show, owner
 *  SUM the votes to make a total
 *  return an array of all votes
 */
router
  .get("/vote", function (req, res) {
    var id = null;
    var dated = null;
    var datef = null;
    var bool = false;
    var sql =
      "SELECT v.show_id,v.owner_id,v.title_id,v.title,v.artist,SUM(v.vote) as note,COUNT(v.vote) as nb_vote, MAX(v.voted_at) as lastvote, s.name,o.login " +
      "FROM chaosradio.votes v " +
      "JOIN public.cc_show AS s ON(v.show_id = s.id) " +
      "join cc_subjs o ON(o.id = v.owner_id ) ";
    if (req.query.owner != null) {
      bool = true;
      id = req.query.owner;
      sql += "WHERE (o.login LIKE $1 ";
    }
    if (req.query.show_id != null) {
      if (bool === true) {
        sql += "AND ";
      } else {
        sql += "WHERE (";
      }
      id = req.query.show_id;
      sql += "v.show_id =$1 ";
      bool = true;
    }
    if (req.query.dated != null && req.query.datef != null) {
      if (bool === true) {
        sql += "AND ";
      } else {
        sql += "WHERE (";
      }
      bool = true;
      dated = req.query.dated;
      datef = req.query.datef;
      sql += "v.voted_at BETWEEN $2 AND $3 ";
    } else if (req.query.dated != null && req.query.datef == null) {
      if (bool === true) {
        sql += "AND ";
      } else {
        sql += "WHERE (";
      }
      bool = true;
      dated = req.query.dated;
      sql += "v.voted_at >= $2 ";
    } else if (req.query.dated == null && req.query.datef != null) {
      if (bool === true) {
        sql += "AND ";
      } else {
        sql += "WHERE (";
      }
      bool = true;
      datef = req.query.datef;
      sql += "v.voted_at <= $3 ";
    }
    if (bool === true) {
      sql += ")";
    }
    sql +=
      " GROUP BY v.title_id,v.show_id,v.owner_id,v.title,v.artist,s.name,o.login";
    var vote = [id, dated, datef];
    db.manyOrNone(sql, vote)
      .then(function (data) {
        if (!data) {
          res.send("no data");
        } else {
          res.send(data);
        }
      })
      .catch(function (error) {
        console.log("ERROR:", error);
        console.log("--------------------------------");
      });
  })

  /**
   * Post methode inserting a new vote.
   * return confirmation message
   */
  .post("/vote", function (req, res) {
    let artist = req.body.artist;
    let title_id = req.body.title_id;
    let title = req.body.title;
    let show_id = req.body.show_id;
    let owner_id = req.body.owner_id;
    let ip = req.connection.remoteAddress;
    let vote_vote = req.body.vote;
    let key = req.body.key;
    if (
      !artist ||
      !title_id ||
      !title ||
      !show_id ||
      !owner_id ||
      !ip ||
      !key
    ) {
      res.send("Au moins un des champs est vide");
      return;
    }
    db.manyOrNone(
      "SELECT vote_id FROM chaosradio.votes WHERE title_id = $1 and key = $2 and voted_at::date = CURRENT_DATE",
      [title_id, key]
    )
      .then(function (data) {
        if (data.length >= 1) {
          res.send("already voted today");
        } else {
          const sql =
            "INSERT INTO chaosradio.votes (show_id,owner_id,title_id,title,artist,ip,vote,key) VALUES ($1,$2,$3,$4,$5,$6,$7,$8) RETURNING vote_id";
          const vote = [
            show_id,
            owner_id,
            title_id,
            title,
            artist,
            ip,
            vote_vote,
            key,
          ];
          db.one(sql, vote)
            .then(function (data) {
              console.log("NEW VOTE");
              console.table(req.body);
              console.log("Création vote id : " + data.vote_id);
              console.log("--------------------------------");
              res.send(data);
            })
            .catch(function (error) {
              console.log("ERROR:", error);
              console.log("--------------------------------");
            });
        }
      })
      .catch(function (error) {
        console.log(error);
      });
  })

  /**
   * DELETE Methode for deleting a vote
   * Need a vote vote_id
   */
  .delete("/vote", function (req, res) {
    let ip = req.ip;
    let id = req.query.id;
    let valeurs = [id, ip];
    let sql =
      "DELETE FROM chaosradio.votes WHERE vote_id = $1 AND ip = $2 RETURNING vote_id";
    db.one(sql, valeurs)
      .then(function (data) {
        console.log("Suppression vote : " + id);
        res.send(data);
        console.log("delete ok");
        console.log("----------------------------------------");
      })
      .catch(function (error) {
        console.log("ERROR:", error);
        console.log("--------------------------------");
      });
  });

/**
 *  GET method to retrieve all votes, order by dates, show, owner
 *  SUM the votes to make a total
 *  return an array of all votes
 */
router.get("/user", function (req, res) {
  var sql = "SELECT login " + "FROM cc_subjs";
  db.manyOrNone(sql)
    .then(function (data) {
      if (!data) {
        res.error("no data");
      } else {
        res.json(data);
      }
    })
    .catch(function (error) {
      console.log("ERROR:", error);
    });
});

/**
 * GET the title info
 * Require title_id
 */
router.get("/titleinfo", function (req, res) {
  let sql =
    "SELECT title,artist,s.name FROM chaosradio.votes as v JOIN public.cc_show AS s ON(v.show_id = s.id) WHERE title_id = $1 limit 1";
  let id = req.query.id;
  db.one(sql, id)
    .then(function (data) {
      if (data) {
        res.send(data);
      } else {
        res.error("no data");
      }
    })
    .catch(function (error) {
      console.log("ERROR = " + error);
    });
});

/**
 * GET the vote info
 * Require title_id
 */
router.get("/titlevote", function (req, res) {
  let sql =
    "SELECT voted_at,vote FROM chaosradio.votes WHERE title_id = $1 ORDER BY voted_at ASC";
  let id = req.query.id;
  db.manyOrNone(sql, id)
    .then(function (data) {
      if (data) {
        res.send(data);
      } else {
        res.error("no data");
      }
    })
    .catch(function (error) {
      console.log("ERROR : " + error);
    });
});

/**
 * Recupere le nombre totale de vote sur la radio
 */
router.get("/admin", function (req, res) {
  let sql = "SELECT COUNT(vote) FROM chaosradio.votes";
  db.one(sql)
    .then(function (data) {
      res.send(data);
    })
    .catch(function (error) {
      console.log("ERROR : " + error);
    });
});

/**
 * Recupere la lise de tous les derniers votes depuis 30 jours.
 */
router.get("/list", function (req, res) {
  let sql = `WITH dates_table AS (
    SELECT voted_at ::date AS date_column FROM chaosradio.votes v
            )
            SELECT series_table.date, COUNT(dates_table.date_column), SUM(COUNT(dates_table.date_column)) OVER (ORDER BY series_table.date) FROM (
                SELECT (last_date - b.offs) AS date
                    FROM (
                        SELECT GENERATE_SERIES(0, last_date - first_date, 1) AS offs, last_date from (
                             SELECT MAX(date_column) AS last_date, (MAX(date_column) - '1 month'::interval)::date AS first_date FROM dates_table
                        ) AS a
                    ) AS b
            ) AS series_table
            LEFT OUTER JOIN dates_table
                ON (series_table.date = dates_table.date_column)
            GROUP BY series_table.date
            ORDER BY series_table.date`;
  db.many(sql)
    .then(function (data) {
      res.send(data);
    })
    .catch(function (error) {
      console.log("ERROR : " + error);
    });
});

/**
 * Recuperer les tops 5
 */
router.get("/top", function (req, res) {
  const sql =
    "Select title, artist, COUNT(vote), SUM(vote) FROM chaosradio.votes WHERE voted_at > CURRENT_DATE - interval '30 days ' GROUP BY title, artist HAVING SUM(vote) > 0 ORDER BY SUM(vote) DESC, MAX(voted_at) ASC limit 5 ";
  db.manyOrNone(sql)
    .then(function (data) {
      res.send(data);
    })
    .catch(function (error) {
      console.log("ERROR : " + error);
    });
});

/**
 * Recuperer les top 3
 */
router.get("/top3", function (req, res) {
  const sql =
    "Select title, artist, COUNT(vote), SUM(vote) FROM chaosradio.votes WHERE voted_at > CURRENT_DATE - interval '7 days ' GROUP BY title, artist HAVING SUM(vote) > 0 ORDER BY SUM(vote) DESC, MAX(voted_at) ASC limit 3 ";
  db.manyOrNone(sql)
    .then(function (data) {
      res.send(data);
    })
    .catch(function (error) {
      console.log("ERROR : " + error);
    });
});

/**
 * Recuperer le Json Chaos modifié
 */
router.get("/get/api", function (req, res) {
  nbAuditeurs++;
  if (resultat) {
    resultat.schedulerTime = new Date().toLocaleDateString("fr-FR", options);
    res.send(resultat);
  } else {
    res.send(null);
  }
});

module.exports = router;

recupSpotify();
